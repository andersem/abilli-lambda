const AWS = require('aws-sdk');

const tableName = "abilli-abilliPolygons-1PG96FZEBV35V";

exports.handler = (event, context, callback) => {
    const ddb = new AWS.DynamoDB({apiVersion: '2012-08-10'});

    const params = {
        TableName: tableName
    };

    ddb.scan(params, function (err, data) {
        if (err) {
            const error = JSON.stringify(err, null, 2);
            console.log("Error", err);
            callback(null, {
                statusCode: '500',
                body: error,
                headers: {
                    "Access-Control-Allow-Origin": "*"
                }
            });
        } else {
            callback(null, {
                statusCode: '200',
                body: JSON.stringify(mapData(data)),
                headers: {
                    "Access-Control-Allow-Origin": "*"
                }
            });
        }
    });
};

const mapData = (data) => {
    const features = data.Items.map(item => {
        return {
            id: item.id.S,
            created: item.created.S,
            feature: JSON.parse(item.data.S)
        }
    });
    return {
        count: data.Count,
        scannedCount: data.ScannedCount,
        features: features
    };
};