## API

### Get polygons

#### Request

`curl https://f5kr2ylkli.execute-api.eu-central-1.amazonaws.com/Prod`

#### Response
```
{
    "count": 1,
    "scannedCount": 1,
    "features": [
        {
            "id": "634d05de-b677-1cfe-9ae7-621101f2cfee",
            "created": "2018-06-05T21:03:58.951Z",
            "feature": {
                "type": "Feature",
                "properties": {},
                "geometry": {
                    "type": "MultiPolygon",
                    "coordinates": [
                        [
                            [
                                [
                                    -0.1352691650390625,
                                    51.51963895991333
                                ],
                                [
                                    -0.1352691650390625,
                                    51.50810140697543
                                ],
                                [
                                    -0.11398315429687499,
                                    51.50810140697543
                                ],
                                [
                                    -0.11398315429687499,
                                    51.51963895991333
                                ],
                                [
                                    -0.1352691650390625,
                                    51.51963895991333
                                ]
                            ]
                        ],
                        [
                            [
                                [
                                    -0.13595581054687497,
                                    51.50510971251776
                                ],
                                [
                                    -0.13595581054687497,
                                    51.49698840879303
                                ],
                                [
                                    -0.11226654052734375,
                                    51.49698840879303
                                ],
                                [
                                    -0.11226654052734375,
                                    51.50510971251776
                                ],
                                [
                                    -0.13595581054687497,
                                    51.50510971251776
                                ]
                            ]
                        ]
                    ]
                },
                "id": 84
            }
        }
    ]
}
```

### Upload polygons

#### Request

```
curl -X POST https://f5kr2ylkli.execute-api.eu-central-1.amazonaws.com/Prod \
  -H 'Content-Type: application/json' \
  -d '{
  "type": "Feature",
  "properties": {},
  "geometry": {
    "type": "Polygon",
    "coordinates": [
      [
        [
          -0.14007568359375,
          51.5027589576403
        ],
        [
          -0.12325286865234374,
          51.5027589576403
        ],
        [
          -0.12325286865234374,
          51.512588580360244
        ],
        [
          -0.14007568359375,
          51.512588580360244
        ],
        [
          -0.14007568359375,
          51.5027589576403
        ]
      ]
    ]
  }
}'
```

#### Response 

```
{
    "message": "Created successfully",
    "id": "ee9340e0-5b62-afe9-b96f-57391d7350e2"
}
```