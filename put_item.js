const AWS = require('aws-sdk');
const dynamodb = new AWS.DynamoDB({apiVersion: '2012-08-10'});
const GJV = require("geojson-validation");

const tableName = "abilli-abilliPolygons-1PG96FZEBV35V";

exports.handler = (event, context, callback) => {
    const feature = JSON.parse(event.body);
    GJV.isFeature(feature, (valid, errs) => {
        if (valid) {
            const featureId = guid();
            dynamodb.putItem({
                TableName: tableName,
                Item: {
                    "id": {
                        S: featureId
                    },
                    "data": {
                        S: JSON.stringify(feature)
                    },
                    "created": {
                        S: new Date().toISOString()
                    }
                }
            }, (err, data) => {
                if (!err) {
                    callback(null, {
                        statusCode: '200',
                        body: JSON.stringify({
                            message: 'Created successfully',
                            id: featureId
                        }),
                        headers: {
                            "Access-Control-Allow-Origin": "*"
                        }
                    });
                } else {
                    console.log(err, err.stack);
                    callback(null, {
                        statusCode: '500',
                        body: JSON.stringify({
                            error: err
                        }),
                        headers: {
                            "Access-Control-Allow-Origin": "*"
                        }
                    });
                }
            })
        } else {
            console.log("Feature validation error", feature, errs);
            const errorMessage = {
                error: errs,
                feature: feature,
            };
            callback(null, {
                statusCode: '400',
                body: JSON.stringify(errorMessage),
                headers: {
                    "Access-Control-Allow-Origin": "*"
                }
            });
        }

    });
};

function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
}

function guid() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}